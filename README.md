# PostmarketOS image builder

The purpose of this repository is to provide a way how to generate an image for
specific *device* based on the [pmbootstrap.cfg](pmbootstrap.cfg) using the
Gilab's Continuous Integration.

This way anyone can test the image on device they own without the need to learn
*pmbootstrap*.

Some knowledge regarding the install is required but by skipping the building
instructions it should lower the bar.
